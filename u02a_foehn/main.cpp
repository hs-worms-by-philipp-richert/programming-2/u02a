#include "cHaarFoehn.h"

// Aufgabe u02a
// Uebungsaufgabe zur Anlage einer Klasse mit String-Attribut
// Philipp Richert
// 20.10.2022

int main() {
	// Klassen instanzierung
	cHaarFoehn foehn1;
	cHaarFoehn foehn2("Dyson 9000", 700, 3, 1);
	cHaarFoehn foehn3("braun Haartrockner", 800.5, 4, 2);

	// Aufruf der ausgabe Methoden
	foehn1.ausgabe();
	foehn2.ausgabe();
	foehn3.ausgabe();

	// Aufruf der eingabe Methoden
	foehn1.eingabe();
	foehn2.eingabe();
	foehn3.eingabe();

	// Aufruf der ausgabe Methoden
	foehn1.ausgabe();
	foehn2.ausgabe();
	foehn3.ausgabe();

	return 0;
}