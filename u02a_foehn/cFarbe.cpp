#include "cFarbe.h"

// getColor
// Wandelt Integer in String gemaess Enum um
string cFarbe::getColor(int colorCode) {
	string colorAusgabe;

	switch (colorCode) {
		case Color::blau:
			colorAusgabe = "blau";
			break;
		case Color::gelb:
			colorAusgabe = "gelb";
			break;
		case Color::rot:
			colorAusgabe = "rot";
			break;
		case Color::schwarz:
			colorAusgabe = "schwarz";
			break;
		case Color::weiss:
			colorAusgabe = "weiss";
			break;
		default:
			colorAusgabe = to_string(colorCode);
			break;
	}

	return colorAusgabe;
}

// getInt Methode
// Wandelt String in Integer gemaess Enum um
int cFarbe::getInt(string color) {
	if (color == "schwarz" || color == "0") return 0;
	else if (color == "rot" || color == "1") return 1;
	else if (color == "blau" || color == "2") return 2;
	else if (color == "gelb" || color == "3") return 3;
	else if (color == "weiss" ||color == "4") return 4;
	else return -1;
}