#pragma once

#include <string>

using namespace std;

class cFarbe
{
private:
	enum Color { schwarz, rot, blau, gelb, weiss };
public:
	string getColor(int);
	int getInt(string);
};

