#pragma once

#include <string>

using namespace std;

class cHaarFoehn
{
private:
	string bezeichnung;
	double leistung;
	int schaltstufen;
	int farbe;
public:
	cHaarFoehn(string = "unbestimmt", double = 0.0, int = 0, int = 0);
	void eingabe();
	void ausgabe();
};

