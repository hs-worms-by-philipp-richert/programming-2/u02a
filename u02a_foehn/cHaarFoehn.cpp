#include "cHaarFoehn.h"
#include "cFarbe.h"
#include <iostream>

// Konstruktor
cHaarFoehn::cHaarFoehn(string bez_in, double leistung_in, int stufen_in, int farbe_in) {
	bezeichnung = bez_in;
	leistung = leistung_in;
	schaltstufen = stufen_in;
	farbe = farbe_in;
}

// eingabe Methode
// Ueberschreibt Attribute mit Werten von der Tastatur
void cHaarFoehn::eingabe() {
	cFarbe farbenGetter;
	string tmpFarbe;

	cout << endl << "Bitte geben Sie neue Daten fuer \"" << bezeichnung << "\" ein:" << endl;
	cout << "\tBezeichnung: ";
	cin >> bezeichnung;

	cout << "\tLeistung: ";
	cin >> leistung;

	cout << "\tSchaltstufen: ";
	cin >> schaltstufen;

	cout << "\tFarbe: ";
	cin >> tmpFarbe;
	cout << endl;

	// Eingbe erlaubt sowohl Zahlen von 0-4, als auch folgende
	// string-Werte: schwarz, rot, blau, gelb, weiss
	farbe = farbenGetter.getInt(tmpFarbe);
}

// ausgabe Methode
// Gibt Attribut-Werte ueber die Standardausgabe aus
void cHaarFoehn::ausgabe() {
	cFarbe farbenGetter;

	cout << endl << "Daten fuer \"" << bezeichnung << "\":" << endl
		<< "\tLeistung: " << leistung << endl
		<< "\tSchaltstufen: " << schaltstufen << endl
		<< "\tFarbe: " << farbenGetter.getColor(farbe) << " (" << farbe << ")" << endl;
}